app.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
	$ionicConfigProvider.tabs.position('bottom');
	// Ionic uses AngularUI Router which uses the concept of states
	// Learn more here: https://github.com/angular-ui/ui-router
	// Set up the various states which the app can be in.
	// Each state's controller can be found in controllers.js
	$stateProvider

	// setup an abstract state for the tabs directive
		.state('tab', {
		url: '/tab',
		abstract: true,
		templateUrl: 'app/setting/views/tabs.html'
	})

	// Each tab has its own nav history stack:

	// .state('tab.dash', {
	//   url: '/dash',
	//   views: {
	//     'tab-dash': {
	//       templateUrl: 'app/core/views/tab-dash.html',
	//       controller: 'DashController'
	//     }
	//   }
	// })

	// .state('tab.chats', {
	//     url: '/chats',
	//     views: {
	//       'tab-chats': {
	//         templateUrl: 'app/chats/views/tab-chats.html',
	//         controller: 'ChatsController'
	//       }
	//     }
	//   })
	//   .state('tab.chat-detail', {
	//     url: '/chats/:chatId',
	//     views: {
	//       'tab-chats': {
	//         templateUrl: 'app/chats/views/chat-detail.html',
	//         controller: 'ChatDetailController'
	//       }
	//     }
	//   })
	.state('tab.dictionary', {
			url: '/dictionary',
			views: {
				'tab-dictionary': {
					templateUrl: 'app/dictionary/views/tab-dictionary.html',
					controller: 'DictionaryController'
				}
			}
		})
		.state('tab.translate', {
			url: '/translate',
			views: {
				'tab-translate': {
					templateUrl: 'app/translate/views/tab-translate.html',
					controller: 'TranslateController'
				}
			}
		})
		.state('tab.settings', {
			url: '/settings',
			views: {
				'tab-settings': {
					templateUrl: 'app/setting/views/tab-settings.html',
					controller: 'SettingsController'
				}
			}
		})
		.state('tab.setting-about', {
			url: '/setting/about',
			views: {
				'tab-settings': {
					templateUrl: 'app/setting/views/tab-about.html',
					controller: 'AboutController'
				}
			}
		});
	// .state('tab.account', {
	//     url: '/account',
	//     views: {
	//       'tab-account': {
	//         templateUrl: 'app/core/views/tab-account.html',
	//         controller: 'AccountController'
	//       }
	//     }
	// });

	// if none of the above states are matched, use this as the fallback
	$urlRouterProvider.otherwise('/tab/translate');

});
