translateModule.factory('Translate', Translate);

function Translate($http, $q, $httpParamSerializerJQLike, $timeout, ApiEndpoint) {
    var Translate = {
        getUserLanguages: _getUserLanguages,
        getWordTranslateDetails: _getWordTranslateDetails,
        getSuggestionWordTranslate: _getSuggestionWordTranslate,
        searchLanguage: _searchLanguage,
        cancel: _cancel,
        translate: _translate,
    };

    return Translate;

    function _cancel(promise) {
        // If the promise does not contain a hook into the deferred timeout,
        // the simply ignore the cancel request.
        if (
            promise &&
            promise._httpTimeout &&
            promise._httpTimeout.resolve
        ) {
            promise._httpTimeout.resolve();
        }
    }


    function _translate($entity) {
        $entity.basic = true; // for mobile
        var defer = $q.defer();
        var request = $http({
            method: "POST",
            url:  ApiEndpoint.url + "/translate/translate",
            data: $httpParamSerializerJQLike($entity),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            timeout: defer.promise
        });

        var promise = request.then(_unwrapResolve);

        promise._httpTimeout = defer;

        return (promise);
    }

    function _unwrapResolve(response) {
        return (response.data);
    }

    function _searchLanguage(languages, searchFilter) {

       var deferred = $q.defer();

       var matches = languages.filter( function(language) {
           if(language.name.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1 ) return true;
       })

       $timeout( function(){

          deferred.resolve( matches );

       }, 100);

       return deferred.promise;

   };

    function _getUserLanguages() {
        var defer = $q.defer();
        $http({
            method: 'GET',
            url: ApiEndpoint.url +'/language/getUserLanguages'
        }).success(function(data) {
            defer.resolve(data);
        }).error(function(data) {
            defer.reject(data);
        });
        return defer.promise;
    }

    function _getSuggestionWordTranslate($val, $lang) {
        $entity = {
            value: $val,
            lang: $lang
        };
        var defer = $q.defer();
        $http({
            method: 'POST',
            url: ApiEndpoint.url +'/word/getSuggestionWordTranslate',
            //crossDomain: true, // enable this
            data: $httpParamSerializerJQLike($entity),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            if (data.content === undefined) {
                //_debug(data);
                defer.reject(data);
            } else {
                defer.resolve(data);
            }
        }).error(function(data) {
            if (data.content.errorCode == 401) {
                // baseCommon.pushToasterMessages([{
                //     message: data.content.messages,
                //     type: 'warning'
                // }]);
                // console.log(data.content);
            } else {
                defer.reject(data);
            }
        });
        return defer.promise;
    }

    function _getWordTranslateDetails($id) {
        $entity = {
            id: $id
        };
        var defer = $q.defer();
        $http({
            method: 'POST',
            url:ApiEndpoint.url + "/translate/getWordTranslateDetails",
            data: $httpParamSerializerJQLike($entity),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            // console.log(data);
            if (data.content === undefined) {
                //_debug(data);
                defer.reject(data);
            } else {
                defer.resolve(data);
            }
        }).error(function(data) {
            if (data.content.errorCode == 401) {
                // baseCommon.pushToasterMessages([{
                //     message: data.content.messages,
                //     type: 'warning'
                // }]);

                // console.log(data.content);
            } else {
                defer.reject(data);
            }
        });
        return defer.promise;
    }
}
