translateModule.controller('TranslateController', translateController);

function translateController($scope, $rootScope, $timeout, Translate) {
    $scope.data = {};
    $scope.data.from = {
        code: "id-ID",
        name:"Indonesia"
    }

    $scope.data.to = {
        code: "jv-NG",
        name:"Ngoko"
    }

    Translate.getUserLanguages().then(function(result) {
        $scope.userLanguages = angular.copy(result.content.model);
        // $scope.fromLanguageList = angular.copy(result.content.model);
        // $scope.toLanguageList = angular.copy(result.content.model);
        // console.log(result.content.model);
    }, function(error) {});

    $scope.initFromLanguage = function() {
    	$scope.fromLanguageList = angular.copy($scope.userLanguages);
    }

    $scope.searchFrom = function() {
    	Translate.searchLanguage($scope.userLanguages, $scope.data.from.name).then(
    		function(matches) {
    			$scope.fromLanguageList = matches;
    		}
    	)
    }

    $scope.selectFromLanguage = function(isEnter) {
        if(isEnter){
            this.fromLanguage = $scope.fromLanguageList[0];
        }
        $scope.fromLanguageList = {};
        $scope.data.from = angular.copy(this.fromLanguage);
        _executor();
    }

    $scope.blurFrom = function() {
        $scope.fromLanguageList = {};
    }

    $scope.initToLanguage = function() {
    	$scope.toLanguageList = angular.copy($scope.userLanguages);
    }

    $scope.searchTo = function() {
    	Translate.searchLanguage($scope.userLanguages, $scope.data.to.name).then(
    		function(matches) {
    			$scope.toLanguageList = matches;
    		}
    	)
    }

    $scope.selectToLanguage = function(isEnter) {
        if(isEnter){
            this.toLanguage = $scope.toLanguageList[0];
        }
        $scope.toLanguageList = {};
        $scope.data.to = angular.copy(this.toLanguage);
        _executor()
    }

    $scope.blurTo = function() {
        $scope.toLanguageList = {};
    }

    $scope.swap = function() {
        var temp = angular.copy($scope.data.from);
        $scope.data.from = angular.copy($scope.data.to)
        $scope.data.to = temp;
        _executor();
    }

    $scope.onSourceChange = _onSourceChange;
    var executionTime = 200; // in milisecond
    var countDown = executionTime;
    var isTimerStarted = false;

    function _onSourceChange(o, $event) {
        // console.log($scope.data);
        // $scope.data.source = $('textarea').val();
        if ($scope.data.source === "" ) {
            $scope.data.result = '';
            Translate.cancel(lastRequest);
            return;
        }
        countDown = executionTime;
        if (!isTimerStarted) {
            isTimerStarted = true;
            _startTimer();
        }
    }

    function _startTimer() {
        $timeout(function() {
            countDown--;
            if (!countDown) {
                isTimerStarted = false;
                _executor();
            } else {
                _startTimer();
            }
        }, 1);
    }

    $scope.$on(
        "$destroy",
        function handleDestroyEvent() {
            Translate.cancel(lastRequest);
        }
    );

    var lastRequest = _executor();

    function _executor() {
        if($scope.data.source === undefined) return;
        $rootScope.isLoading = true;
        // $('textarea').val($('textarea').val());//.replace(/ +(?= )/g, ''));
        Translate.cancel(lastRequest);
        var $from = angular.copy($scope.data.from.code);
        var $to = angular.copy($scope.data.to.code);
        var entity = {
            from:angular.copy($scope.data.from.code),
            to:angular.copy($scope.data.to.code),
            source:angular.copy($scope.data.source)
        }
        lastRequest = Translate.translate(entity);
        lastRequest.then(
            function handleTranslateResolve(result) {
                if (result.content === undefined) {
                    // baseCommon.pushMessages(result);
                    // console.log(result);
                } else {
                    $scope.data.result = result.content.model;
                    console.log($scope.data.result);
                }
                $rootScope.isLoading = false;
            },
            function handleTranslateReject(result) {
                if (!result.config) {
                    // baseCommon.pushMessages(result);
                    console.log(result);
                }
            }
        );
        return (lastRequest);
    }
};
