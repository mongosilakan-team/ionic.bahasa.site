// Ionic Starter App

//"proxyUrl": "http://devmongo:4040/api"
// "proxyUrl": "http://mongosilakan.net/bahasa.site.api/api"

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js

var app = angular.module('mongoapp', [
	// 3rd party lib
	'ionic',
	'admobModule',
	// app mondule
	'app.settings',
	'app.chats',
	'app.dictionary',
	'app.translate'
]).constant('ApiEndpoint', {
	// url: '/api/v1'
		url: 'http://mongosilakan.net/api/v1'
}).run(function($state, $ionicPopup, $ionicPlatform, $ionicHistory, $rootScope) {
	$rootScope.isLoading = false;
	$rootScope.appVersion = "1.0.0";
	$ionicPlatform.ready(function() {
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			cordova.plugins.Keyboard.disableScroll(true);

		}
		if (window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
		}
		if (window.cordova) {
			cordova.getAppVersion(function(version) {
				$rootScope.appVersion = version;
			});
		}
	});

	$ionicPlatform.registerBackButtonAction(function(event) {
		if ($state.current.name == "tab.translate") {
			//<-- remove this line to disable the exit
			var confirmPopup = $ionicPopup.confirm({
				title: 'Konfirmasi Keluar',
				template: 'Anda yakin untuk keluar?'
			});
			confirmPopup.then(function(res) {
				if (res) {
					navigator.app.exitApp();
				}
			});
		} else {
			$ionicHistory.goBack();
		}
	}, 100);
});
