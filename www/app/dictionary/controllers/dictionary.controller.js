dictionaryModule.controller('DictionaryController', dictionaryController);

function dictionaryController($scope, $rootScope, $timeout, $ionicLoading, Dictionary) {

	$scope.activeAndPendingLanguageList = {};
	Dictionary.getActiveAndPendingLanguage().then(function(result) {
		$scope.activeAndPendingLanguageList = result.content.model;
	}, function(error) {});

	$scope.onSourceChange = _onSourceChange;
	var executionTime = 200; // in milisecond
	var countDown = executionTime;
	var isTimerStarted = false;

	function _onSourceChange(o, $event) {
		$scope.source = this.source;
		// $scope.data.source = $('textarea').val();
		if (!this.source || this.source == '') {
			$scope.suggestionResult = {};
			return;
		}
		countDown = executionTime;
		if (!isTimerStarted) {
			isTimerStarted = true;
			_startTimer();
		}
	}

	function _startTimer() {
		$timeout(function() {
			countDown--;
			if (!countDown) {
				isTimerStarted = false;
				_executor();
			} else {
				_startTimer();
			}
		}, 1);
	}

	function _executor() {
        $scope.searchLoading = true;
		if ($scope.source === undefined || $scope.source == '') return;
		var allLang = 0;
		Dictionary.getSuggestionWordDictionary($scope.source, allLang).then(function(result) {
			$scope.suggestionResult = result.content.model;
            $scope.searchLoading = false;
		}, function(error) {});
	}

	$scope.search = function() {
		if (!this.data.source || this.data.source == '') {
			$scope.suggestionResult = {};
			return;
		};
		var allLang = 0;
		Dictionary.getSuggestionWordDictionary(this.data.source, allLang).then(function(result) {
			$scope.suggestionResult = result.content.model;
		}, function(error) {});
	}

	$scope.wordsToFilter = function() {
		indexedTeams = [];
		return $scope.wordDetails;
	}

	$scope.filterTeams = function(player) {
		var teamIsNew = indexedTeams.indexOf(player.result_language_id) == -1;
		if (teamIsNew) {
			indexedTeams.push(player.result_language_id);
		}
		return teamIsNew;
	}

	$scope.getWordDictionaryDetails = function(isEnter) {
        $rootScope.isLoading = true;
		$scope.wordDetails = [];
		if (isEnter) {
			this.suggestion = $scope.suggestionResult[0];
		}

		$scope.source = this.suggestion;
		$scope.suggestionResult = {};
		var source = this.suggestion;
		Dictionary.getWordDictionaryDetails(this.suggestion.id).then(function(result) {
			result.content.model.member.unshift({
				result_language_id: source.language_id,
				result: source.word,
				id: source.id
			});

            $rootScope.isLoading = false;
			$scope.wordDetails = result.content.model.member;
		}, function(error) {});
	}

	// $scope.show = function() {
	// 	$ionicLoading.show({
	// 		template: '<div>loading</div>'
	// 	}).then(function() {
	// 		console.log("The loading indicator is now displayed");
	// 	});
	// };
	// $scope.hide = function() {
	// 	$ionicLoading.hide().then(function() {
	// 		console.log("The loading indicator is now hidden");
	// 	});
	// };
};
