dictionaryModule.factory('Dictionary', Dictionary);

function Dictionary($http, $q, $httpParamSerializerJQLike, ApiEndpoint) {
    var Dictionary = {
        getActiveAndPendingLanguage: _getActiveAndPendingLanguage,
        getWordDictionaryDetails: _getWordDictionaryDetails,
        getSuggestionWordDictionary: _getSuggestionWordDictionary,
    };

    return Dictionary;

    function _getActiveAndPendingLanguage() {
        var defer = $q.defer();
        $http({
            method: 'GET',
             url: ApiEndpoint.url +'/language/getActiveAndPending'
            //url: 'http://mongosilakan.net/bahasa.site.api/api/language/getActiveAndPending'
        }).success(function(data) {
            defer.resolve(data);
        }).error(function(data) {
            defer.reject(data);
        });
        return defer.promise;
    }

    function _getSuggestionWordDictionary($val, $lang) {
        $entity = {
            value: $val,
            lang: $lang
        };
        var defer = $q.defer();
        $http({
            method: 'POST',
            url: ApiEndpoint.url +'/word/getSuggestionWordDictionary',
            //url: 'http://mongosilakan.net/bahasa.site.api/api/word/getSuggestionWordDictionary',
            //crossDomain: true, // enable this
            data: $httpParamSerializerJQLike($entity),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            if (data.content === undefined) {
                //_debug(data);
                defer.reject(data);
            } else {
                defer.resolve(data);
            }
        }).error(function(data) {
            if (data.content.errorCode == 401) {
                baseCommon.pushToasterMessages([{
                    message: data.content.messages,
                    type: 'warning'
                }]);
            } else {
                defer.reject(data);
            }
        });
        return defer.promise;
    }

    function _getWordDictionaryDetails($id) {
        $entity = {
            id: $id
        };
        var defer = $q.defer();
        $http({
            method: 'POST',
            url:ApiEndpoint.url + "/translate/getWordDictionaryDetails",
            data: $httpParamSerializerJQLike($entity),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            // console.log(data);
            if (data.content === undefined) {
                //_debug(data);
                defer.reject(data);
            } else {
                defer.resolve(data);
            }
        }).error(function(data) {
            if (data.content.errorCode == 401) {
                baseCommon.pushToasterMessages([{
                    message: data.content.messages,
                    type: 'warning'
                }]);
            } else {
                defer.reject(data);
            }
        });
        return defer.promise;
    }
}
